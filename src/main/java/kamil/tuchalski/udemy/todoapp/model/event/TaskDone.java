package kamil.tuchalski.udemy.todoapp.model.event;

import kamil.tuchalski.udemy.todoapp.model.Task;

import java.time.Clock;

public class TaskDone extends TaskEvent {
    TaskDone(final Task source) {
        super(source.getId(), Clock.systemDefaultZone());
    }
}
