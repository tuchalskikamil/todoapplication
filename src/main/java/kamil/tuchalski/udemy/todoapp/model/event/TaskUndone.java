package kamil.tuchalski.udemy.todoapp.model.event;

import kamil.tuchalski.udemy.todoapp.model.Task;

import java.time.Clock;

public class TaskUndone extends TaskEvent {
    TaskUndone(final Task source) {
        super(source.getId(), Clock.systemDefaultZone());
    }
}
