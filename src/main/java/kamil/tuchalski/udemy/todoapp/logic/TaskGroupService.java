package kamil.tuchalski.udemy.todoapp.logic;

import kamil.tuchalski.udemy.todoapp.model.Project;
import kamil.tuchalski.udemy.todoapp.model.TaskGroup;
import kamil.tuchalski.udemy.todoapp.model.TaskGroupRepository;
import kamil.tuchalski.udemy.todoapp.model.TaskRepository;
import kamil.tuchalski.udemy.todoapp.model.projection.GroupReadModel;
import kamil.tuchalski.udemy.todoapp.model.projection.GroupWriteModel;

import java.util.List;
import java.util.stream.Collectors;


public class TaskGroupService {
    private TaskGroupRepository repository;
    private TaskRepository taskRepository;

    TaskGroupService(final TaskGroupRepository repository,
                     final TaskRepository taskRepository) {
        this.repository = repository;
        this.taskRepository = taskRepository;
    }

    public GroupReadModel createGroup(final GroupWriteModel source) {
        return createGroup(source, null);
    }

    GroupReadModel createGroup(final GroupWriteModel source, final Project project) {
        TaskGroup result = repository.save(source.toGroup(project));
        return new GroupReadModel(result);
    }

    public List<GroupReadModel> readAll() {
        return repository.findAll().stream()
                .map(GroupReadModel::new)
                .collect(Collectors.toList());
    }

    public void toggleGroup(int groupId) {
        if (taskRepository.existsByDoneIsFalseAndGroup_Id(groupId)) {
            throw new IllegalStateException("Group has undone tasks. Done all the tasks first");
        }
        TaskGroup result = repository.findById(groupId).orElseThrow(() -> new IllegalArgumentException("TaskGroup " +
                "with given id not found"));
        result.setDone(!result.isDone());
        repository.save(result);
    }

}
