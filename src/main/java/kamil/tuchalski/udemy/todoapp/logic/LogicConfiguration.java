package kamil.tuchalski.udemy.todoapp.logic;

import kamil.tuchalski.udemy.todoapp.TaskConfigurationProperties;
import kamil.tuchalski.udemy.todoapp.model.ProjectRepository;
import kamil.tuchalski.udemy.todoapp.model.TaskGroupRepository;
import kamil.tuchalski.udemy.todoapp.model.TaskRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogicConfiguration {
    @Bean
    public ProjectService service(
            final ProjectRepository repository,
            final TaskGroupRepository taskGroupRepository,
            final TaskConfigurationProperties config,
            final TaskGroupService taskGroupService
    ) {
        return new ProjectService(repository, taskGroupRepository, taskGroupService,config );
    }

    @Bean
    public TaskGroupService taskGroupService(
            final TaskGroupRepository taskGroupRepository,
            final TaskRepository taskRepository
    ) {
        return new TaskGroupService(taskGroupRepository, taskRepository);
    }
}
