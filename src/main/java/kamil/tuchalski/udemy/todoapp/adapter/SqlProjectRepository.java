package kamil.tuchalski.udemy.todoapp.adapter;

import kamil.tuchalski.udemy.todoapp.model.Project;
import kamil.tuchalski.udemy.todoapp.model.ProjectRepository;
import kamil.tuchalski.udemy.todoapp.model.TaskGroup;
import kamil.tuchalski.udemy.todoapp.model.TaskGroupRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SqlProjectRepository extends ProjectRepository, JpaRepository<Project, Integer> {
    @Override
    @Query("from Project p join fetch p.steps")
    List<Project> findAll();

}
